const tasks = [
  {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
  {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
  {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
  {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
  {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
  {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
  {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const frontEndTasks = tasks.filter(task => task.category === 'Frontend');
const backEndTasks = tasks.filter(task => task.category === 'Backend');
const bugs = tasks.filter(task => task.type === 'bug');
const tasksWithUiInTitle = tasks.filter(task => task.title.includes('UI'));
const moreThenFourHourTasks = tasks.filter(task => task.timeSpent > 4);
const moreThenFourHourTasksSerialized = moreThenFourHourTasks.map(task => {
  return {title: task.title, category: task.category}
});

const calculateTime = function (tasks) {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const tasksTimeSpent = tasks.map(task => task.timeSpent);
  return tasksTimeSpent.reduce(reducer)
};

const timeSpended = {
  frontEnd: calculateTime(frontEndTasks),
  backEnd: calculateTime(backEndTasks),
  bugs: calculateTime(bugs)
};

console.log(`FrontEnd time spent: ${timeSpended.frontEnd}`);
console.log(`Bugs time spent: ${timeSpended.bugs}`);
console.log(`Tasks with UI in title: ${JSON.stringify(tasksWithUiInTitle)}`);
console.log(`Time Spent: ${JSON.stringify(timeSpended)}`);
console.log(`More then four hour tasks: ${JSON.stringify(moreThenFourHourTasksSerialized)}`);


