import React, { Component } from 'react';
import './App.css';
import Uniqid, {} from 'uniqid';

class App extends Component {
  state = {
    items: [],
    total: 0
  };

  addItem = () => {
    const items = [...this.state.items];
    let total = this.state.total;
    const item = { id: Uniqid(), name: this.itemName.value, cost: parseInt(this.itemCost.value) };
    if (item.name === '') item.name = 'no name';
    if (isNaN(item.cost)) item.cost = 0;
    this.itemName.value = null;
    this.itemCost.value = null;
    total += item.cost;
    items.push(item);
    this.setState({items, total})
  };

  removeItem = event => {
    const items = [...this.state.items];
    let total = this.state.total;
    const item = items.findIndex(item => ('delete-' + item.id) === event.currentTarget.id);
    total -= items[item].cost;
    items.splice(item, 1);
    this.setState({items, total});
  };

  render() {
    let itemList = this.state.items.map((item) => (
      <div className="item" key={item.id}>
        {item.name} {item.cost} <button id={"delete-" + item.id}
                                        type="button"
                                        onClick={(event) => this.removeItem(event)}>x</button>
      </div>
    ));
    return (
      <div className="App">
        <div className="item-form">
          <input type="text" placeholder="Item name" ref={input => {this.itemName = input}}/>
          <input type="text" placeholder="cost" ref={input => {this.itemCost = input}}/>
          <button type="button" onClick={this.addItem}>Add</button>
        </div>
        <div className="item-list">
          Total spent: {this.state.total} KGS
          {itemList}
        </div>
      </div>
    );
  }
}

export default App;
